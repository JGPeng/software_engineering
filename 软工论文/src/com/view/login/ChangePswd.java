package com.view.login;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import main.Constant;
//修改密码界面
public class ChangePswd {
	private JFrame changePswd;
	private JLabel title,usernameLabel,oldPswdLabel,newPswdLabel,renewPswdLabel;
	private JTextField username,oldPswd,newPswd,renewPswd;
	private JButton changeBtn,rewriteBtn;
	
	public ChangePswd() {
		changePswd=new JFrame("修改用户密码");
		changePswd.setSize(Constant.LOGIN_WIDTH,Constant.LOGIN_HEIGHT);
		changePswd.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		changePswd.setResizable(false);
		changePswd.setLocationRelativeTo(null);
		changePswd.getContentPane().setLayout(null);
		
		title = new JLabel("修改用户密码",JLabel.CENTER);
		title.setFont(new Font("宋体", Font.BOLD, 25));
		title.setBounds(0, 10, Constant.LOGIN_WIDTH-10, 50);
		changePswd.getContentPane().add(title);
		
		usernameLabel = new JLabel("用户ID：",JLabel.RIGHT);
		usernameLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		usernameLabel.setBounds(5, 70, 100, 30);
		changePswd.getContentPane().add(usernameLabel);
		
		username = new JTextField();
		username.setFont(new Font("宋体", Font.PLAIN, 14));
		username.setBounds(115, 70, 150, 30);
		changePswd.getContentPane().add(username);
		username.setColumns(10);
		
		oldPswdLabel = new JLabel("旧密码：",JLabel.RIGHT);
		oldPswdLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		oldPswdLabel.setBounds(5, 110, 100, 30);
		changePswd.getContentPane().add(oldPswdLabel);
		
		oldPswd = new JTextField();
		oldPswd.setFont(new Font("宋体", Font.PLAIN, 14));
		oldPswd.setBounds(115, 110, 150, 30);
		changePswd.getContentPane().add(oldPswd);
		oldPswd.setColumns(10);
		
		newPswdLabel = new JLabel("新密码：",JLabel.RIGHT);
		newPswdLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		newPswdLabel.setBounds(5, 150, 100, 30);
		changePswd.getContentPane().add(newPswdLabel);
		
		newPswd = new JTextField();
		newPswd.setFont(new Font("宋体", Font.PLAIN, 14));
		newPswd.setBounds(115, 150, 150, 30);
		changePswd.getContentPane().add(newPswd);
		newPswd.setColumns(10);
		
		renewPswdLabel = new JLabel("确认新密码：",JLabel.RIGHT);
		renewPswdLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		renewPswdLabel.setBounds(5, 190, 100, 30);
		changePswd.getContentPane().add(renewPswdLabel);
		
		renewPswd = new JTextField();
		renewPswd.setFont(new Font("宋体", Font.PLAIN, 14));
		renewPswd.setBounds(115, 190, 150, 30);
		changePswd.getContentPane().add(renewPswd);
		renewPswd.setColumns(10);
		
		changeBtn = new JButton("修改");
		changeBtn.setFont(new Font("宋体", Font.PLAIN, 16));
		changeBtn.setBounds(65, 255, 100, 40);
		changePswd.getContentPane().add(changeBtn);
		
		rewriteBtn = new JButton("重置");
		rewriteBtn.setFont(new Font("宋体", Font.PLAIN, 16));
		rewriteBtn.setBounds(175, 255, 100, 40);
		changePswd.getContentPane().add(rewriteBtn);
		
		
		changePswd.setVisible(true);
	}

	public JFrame getChangePswd() {
		return changePswd;
	}
}
