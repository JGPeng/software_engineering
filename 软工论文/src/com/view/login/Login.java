package com.view.login;

import main.Constant;
import javax.swing.*;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//��½����
public class Login{
	private JFrame login=null;
	private JButton landBtn,rewriteBtn,registerBtn,changeBtn;
	private JLabel title,accountText,passwordText,prompt;
	private ButtonGroup bgroup;
	private JRadioButton userBtn,auserBtn;
	private JTextField account,password;
	private JFrame changePswd=null,register=null;

	public Login() {
		login=new JFrame("�û���½");
		login.setSize(Constant.LOGIN_WIDTH,Constant.LOGIN_HEIGHT);
		login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		login.setResizable(false);
		login.setLocationRelativeTo(null);
		login.getContentPane().setLayout(null);
		
		title = new JLabel("�û���½",JLabel.CENTER);
		title.setFont(new Font("����", Font.BOLD, 25));
		title.setBounds(100, 10, 150, 50);
		login.getContentPane().add(title);
		
		accountText = new JLabel("�û�����",JLabel.RIGHT);
		accountText.setFont(new Font("����", Font.PLAIN, 14));
		accountText.setBounds(10, 80, 80, 30);
		login.getContentPane().add(accountText);
		
		passwordText = new JLabel("���룺",JLabel.RIGHT);
		passwordText.setFont(new Font("����", Font.PLAIN, 14));
		passwordText.setBounds(10, 120, 80, 30);
		login.getContentPane().add(passwordText);
		
		account = new JTextField();
		account.setColumns(10);
		account.setBounds(101, 81, 230, 30);
		login.getContentPane().add(account);
		
		password = new JTextField();
		password.setColumns(10);
		password.setBounds(101, 121, 230, 30);
		login.getContentPane().add(password);

		
		landBtn=new JButton("��¼");
		landBtn.setFont(new Font("����", Font.PLAIN, 14));
		landBtn.setBounds(73,175,90,30);
		landBtn.addActionListener(new LandBtnListener());
		rewriteBtn = new JButton("��д");
		rewriteBtn.setFont(new Font("����", Font.PLAIN, 14));
		rewriteBtn.setBounds(200, 175, 90, 30);
		rewriteBtn.addActionListener(new RewriteBtnListener());
		login.getContentPane().add(landBtn);
		login.getContentPane().add(rewriteBtn);
		
		prompt = new JLabel("��ѡ�����ĵ�½��ʽ��");
		prompt.setBounds(10, 214, 150, 20);
		login.getContentPane().add(prompt);
		
		userBtn = new JRadioButton("�û�");
		userBtn.setFont(new Font("����", Font.PLAIN, 14));
		userBtn.setBounds(40, 240, 80, 30);
		userBtn.setSelected(true);
		login.getContentPane().add(userBtn);
		
		auserBtn = new JRadioButton("����Ա");
		auserBtn.setFont(new Font("����", Font.PLAIN, 14));
		auserBtn.setBounds(135, 240, 80, 30);
		login.getContentPane().add(auserBtn);
		
		bgroup=new ButtonGroup();
		bgroup.add(userBtn);
		bgroup.add(auserBtn);
		
		registerBtn = new JButton("ע��");
		registerBtn.setFont(new Font("����", Font.PLAIN, 12));
		registerBtn.setBounds(170, 295, 80, 20);
		registerBtn.addActionListener(new RegisterBtnListener());
		login.getContentPane().add(registerBtn);
		
		changeBtn = new JButton("�޸�����");
		changeBtn.setFont(new Font("����", Font.PLAIN, 12));
		changeBtn.setBounds(270, 295, 80, 20);
		changeBtn.addActionListener(new ChangeBtnListener());
		login.getContentPane().add(changeBtn);
		
		login.setVisible(true);
	}
	
	public JFrame getLogin() {
		return login;
	}
	
	//��½��ť������
	class LandBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
//			System.out.println("user:"+bgroup.isSelected(userBtn.getModel()));
		}
	}
	
	//��д��ť������
	class RewriteBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			if(!account.getText().equals("")) {
				account.setText("");
			}
			if(!password.getText().equals("")) {
				password.setText("");
			}
		}
	}
	
	//�޸�����
	class ChangeBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			if(changePswd==null) {
				changePswd=new ChangePswd().getChangePswd();
			}
			changePswd.setVisible(true);
		}
	}
	
	//ע��
	class RegisterBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent arg) {
			if(register==null) {
				register=new Register().getRegister();
			}
			register.setVisible(true);
		}
	}
}
