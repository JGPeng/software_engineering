package com.view.login;

import javax.swing.JFrame;
import main.Constant;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
//注册界面
public class Register {
	private JFrame register;
	private JLabel title,usernameLabel,emailLabel,passwordLabel,repasswordLabel;
	private JTextField username,email,password,repassword;
	private JButton registerBtn,rewriteBtn;
	
	public Register() {
		register=new JFrame("用户登陆");
		register.setSize(Constant.LOGIN_WIDTH,Constant.LOGIN_HEIGHT);
		register.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		register.setResizable(false);
		register.setLocationRelativeTo(null);
		register.getContentPane().setLayout(null);
		
		title = new JLabel("用户注册",JLabel.CENTER);
		title.setFont(new Font("宋体", Font.BOLD, 25));
		title.setBounds(0, 10, Constant.LOGIN_WIDTH-10, 50);
		register.getContentPane().add(title);
		
		usernameLabel = new JLabel("用户ID：",JLabel.RIGHT);
		usernameLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		usernameLabel.setBounds(5, 70, 100, 30);
		register.getContentPane().add(usernameLabel);
		
		username = new JTextField();
		username.setFont(new Font("宋体", Font.PLAIN, 14));
		username.setBounds(115, 70, 150, 30);
		register.getContentPane().add(username);
		username.setColumns(10);
		
		emailLabel = new JLabel("邮箱：",JLabel.RIGHT);
		emailLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		emailLabel.setBounds(5, 110, 100, 30);
		register.getContentPane().add(emailLabel);
		
		email = new JTextField();
		email.setFont(new Font("宋体", Font.PLAIN, 14));
		email.setBounds(115, 110, 150, 30);
		register.getContentPane().add(email);
		email.setColumns(10);
		
		passwordLabel = new JLabel("密码：",JLabel.RIGHT);
		passwordLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		passwordLabel.setBounds(5, 150, 100, 30);
		register.getContentPane().add(passwordLabel);
		
		password = new JTextField();
		password.setFont(new Font("宋体", Font.PLAIN, 14));
		password.setBounds(115, 150, 150, 30);
		register.getContentPane().add(password);
		password.setColumns(10);
		
		repasswordLabel = new JLabel("确认密码：",JLabel.RIGHT);
		repasswordLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		repasswordLabel.setBounds(5, 190, 100, 30);
		register.getContentPane().add(repasswordLabel);
		
		repassword = new JTextField();
		repassword.setFont(new Font("宋体", Font.PLAIN, 14));
		repassword.setBounds(115, 190, 150, 30);
		register.getContentPane().add(repassword);
		repassword.setColumns(10);
		
		registerBtn = new JButton("注册");
		registerBtn.setFont(new Font("宋体", Font.PLAIN, 16));
		registerBtn.setBounds(65, 255, 100, 40);
		register.getContentPane().add(registerBtn);
		
		rewriteBtn = new JButton("重置");
		rewriteBtn.setFont(new Font("宋体", Font.PLAIN, 16));
		rewriteBtn.setBounds(175, 255, 100, 40);
		register.getContentPane().add(rewriteBtn);
		
		
		register.setVisible(true);
	}

	public JFrame getRegister() {
		return register;
	}
}
