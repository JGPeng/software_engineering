package com.view.admin;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import main.Constant;
//管理员系统界面
public class AdminIndex {
	private JFrame admin;
	private JPanel titlePanel,centerPanel,buttonPanel,mainPanel;
	private JLabel title;
	private JButton rentGoodsManageBtn,borrowGoodsManageBtn,orderManageBtn,userManageBtn;
	private RentGoodsManagePanel rentGoodsManagePanel=null;
	private BorrowGoodsManagePanel borrowGoodsManagePanel=null;
	private OrderManagePanel orderManagePanel=null;
	private UserManagePanel userManagePanel=null;
	
	public AdminIndex() {
		admin=new JFrame("管理员界面");
		admin.setSize(Constant.WIDTH,Constant.HEIGHT);
		admin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		admin.setResizable(false);
		admin.setLocationRelativeTo(null);
		
		titlePanel = new JPanel();
		titlePanel.setBackground(new Color(51, 204, 255));
		titlePanel.setPreferredSize(new Dimension(Constant.TITLE_WIDTH, Constant.TITLE_HEIGHT));
		admin.getContentPane().add(titlePanel, BorderLayout.NORTH);
		titlePanel.setLayout(null);
		
		title = new JLabel("管理员界面",JLabel.CENTER);
		title.setFont(new Font("宋体", Font.BOLD, 26));
		title.setBounds(0, 0, 790, 60);
		titlePanel.add(title);
		
		centerPanel = new JPanel();
		admin.getContentPane().add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new BorderLayout());
		
		buttonPanel = new JPanel();
		buttonPanel.setPreferredSize(new Dimension(Constant.BTN_WIDTH, Constant.BTN_HEIGHT));
		centerPanel.add(buttonPanel, BorderLayout.WEST);
		
		rentGoodsManageBtn = new JButton("出租物品管理界面");
		rentGoodsManageBtn.setFont(new Font("宋体", Font.PLAIN, 13));
		rentGoodsManageBtn.setPreferredSize(new Dimension(145, 30));
		rentGoodsManageBtn.addActionListener(new RentGoodsManageListener());
		buttonPanel.add(rentGoodsManageBtn);
		
		borrowGoodsManageBtn = new JButton("租借物品管理界面");
		borrowGoodsManageBtn.setFont(new Font("宋体", Font.PLAIN, 13));
		borrowGoodsManageBtn.setPreferredSize(new Dimension(145, 30));
		borrowGoodsManageBtn.addActionListener(new BorrowGoodsManageListener());
		buttonPanel.add(borrowGoodsManageBtn);
		
		orderManageBtn = new JButton("订单管理界面");
		orderManageBtn.setFont(new Font("宋体", Font.PLAIN, 13));
		orderManageBtn.setPreferredSize(new Dimension(145, 30));
		orderManageBtn.addActionListener(new OrderManageListener());
		buttonPanel.add(orderManageBtn);
		
		userManageBtn = new JButton("用户管理界面");
		userManageBtn.setFont(new Font("宋体", Font.PLAIN, 13));
		userManageBtn.setPreferredSize(new Dimension(145, 30));
		userManageBtn.addActionListener(new UserManageListener());
		buttonPanel.add(userManageBtn);

		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		centerPanel.add(mainPanel, BorderLayout.CENTER);
		
		rentGoodsManagePanel=new RentGoodsManagePanel();
		borrowGoodsManagePanel=new BorrowGoodsManagePanel();
		orderManagePanel=new OrderManagePanel();
		userManagePanel=new UserManagePanel();
		mainPanel.add(rentGoodsManagePanel.getRentGoodsManage(),BorderLayout.CENTER);
		
		admin.setVisible(true);
	}

	
	public JFrame getUser() {
		return admin;
	}
	
	//出租物品管理界面按钮侦听器
	class RentGoodsManageListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			mainPanel.add(rentGoodsManagePanel.getRentGoodsManage(),BorderLayout.CENTER);
			orderManagePanel.getOrderManage().setVisible(false);
			borrowGoodsManagePanel.getBorrowGoodsManage().setVisible(false);
			userManagePanel.getUserManage().setVisible(false);
			rentGoodsManagePanel.getRentGoodsManage().setVisible(true);
		}
	}
	
	//租借物品管理界面按钮侦听器
	class BorrowGoodsManageListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			mainPanel.add(borrowGoodsManagePanel.getBorrowGoodsManage(),BorderLayout.CENTER);
			rentGoodsManagePanel.getRentGoodsManage().setVisible(false);
			orderManagePanel.getOrderManage().setVisible(false);
			userManagePanel.getUserManage().setVisible(false);
			borrowGoodsManagePanel.getBorrowGoodsManage().setVisible(true);
		}
	}
	
	//订单管理界面按钮侦听器
	class OrderManageListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			mainPanel.add(orderManagePanel.getOrderManage(),BorderLayout.CENTER);
			rentGoodsManagePanel.getRentGoodsManage().setVisible(false);
			borrowGoodsManagePanel.getBorrowGoodsManage().setVisible(false);
			userManagePanel.getUserManage().setVisible(false);
			orderManagePanel.getOrderManage().setVisible(true);
		}
	}
	
	//用户管理界面按钮侦听器
	class UserManageListener implements ActionListener{
		public void actionPerformed(ActionEvent arg0) {
			mainPanel.add(userManagePanel.getUserManage(),BorderLayout.CENTER);
			rentGoodsManagePanel.getRentGoodsManage().setVisible(false);
			borrowGoodsManagePanel.getBorrowGoodsManage().setVisible(false);
			orderManagePanel.getOrderManage().setVisible(false);
			userManagePanel.getUserManage().setVisible(true);
		}
	}
}
