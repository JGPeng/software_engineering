package com.view.admin;

import java.awt.*;
import javax.swing.*;
import main.Constant;
/**
 * 出租物品管理界面:
 * 其中分为审核通过的可出租物品和未进行审核的物品；
 * 审核通过的物品会显示ID、物品名、描述、租金/天、押金、剩余数量、出租用户ID的信息；
 * 未进行审核的物品,即未能进行出租的物品，会显示ID、物品名、描述、租金/天、押金、数量、出租用户ID的信息。
 */
public class RentGoodsManagePanel {
	private JPanel rentGoodsManage,auditApproved,notReviewed;
	private JTabbedPane tabbedPane;

	public RentGoodsManagePanel() {
		rentGoodsManage=new JPanel();
		rentGoodsManage.setLayout(new BorderLayout());
		rentGoodsManage.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));

		tabbedPane=new JTabbedPane(JTabbedPane.TOP);
		rentGoodsManage.add(tabbedPane,BorderLayout.CENTER);
		auditApproved=new AuditApprovedPanel().getAuditApproved();
		notReviewed=new NotReviewedPanel().getAuditApproved();
		tabbedPane.add("审核通过物品",auditApproved);
		tabbedPane.add("未审核物品",notReviewed);
	}

	public JPanel getRentGoodsManage() {
		return rentGoodsManage;
	}
}
