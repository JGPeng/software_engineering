package com.view.admin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.util.FitTableColumns;
import com.view.user.ReturnFrame;

import main.Constant;
/**
 * 租借物品管理界面：
 * 显示已归还物品的交易信息，管理员对这些物品进行检查，如果物品正常则点击确认归还。否则退回物品并扣除押金。
 */
public class BorrowGoodsManagePanel {
	private JPanel borrowGoodsManage,searchPanel,mainPanel,btnPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JButton searchBtn,receiveBtn,backBtn;
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel model;
	private String[] head= {"订单ID","出租物品ID","租借用户ID","租借天数","租借时间"};
	

	public BorrowGoodsManagePanel(){
		borrowGoodsManage=new JPanel();
		borrowGoodsManage.setLayout(new BorderLayout());
		borrowGoodsManage.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));
		
		searchPanel = new JPanel();
		searchPanel.setPreferredSize(new Dimension(0,50));
		borrowGoodsManage.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(null);
		
		searchLabel = new JLabel("输入订单ID进行查询：");
		searchLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		searchLabel.setBounds(10, 10, 150, 30);
		searchPanel.add(searchLabel);
		
		search = new JTextField();
		search.setColumns(10);
		search.setBounds(170, 10, 150, 30);
		searchPanel.add(search);
		
		searchBtn = new JButton("查询");
		searchBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		searchBtn.setBounds(540, 10, 80, 30);
		searchBtn.addActionListener(new SearchListener());
		searchPanel.add(searchBtn);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		borrowGoodsManage.add(mainPanel, BorderLayout.CENTER);

		scrollPane = new JScrollPane();
		table = new JTable();
		model=(DefaultTableModel) table.getModel();
		model.setDataVector(null, head);
		scrollPane.setViewportView(table);
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();//单元格内容居中对齐
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, dtcr);
		new FitTableColumns(table);//根据内容自动调节表格的列宽度
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(0, 50));
		borrowGoodsManage.add(btnPanel, BorderLayout.SOUTH);
		
		receiveBtn = new JButton("物品正常，确认归还");
		receiveBtn.setPreferredSize(new Dimension(180, 30));
		receiveBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		receiveBtn.addActionListener(new ReceiveBtnListener());
		btnPanel.add(receiveBtn);
		
		backBtn = new JButton("物品损坏，退回物品");
		backBtn.setPreferredSize(new Dimension(180, 30));
		backBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		backBtn.addActionListener(new BackBtnListener());
		btnPanel.add(backBtn);
	}
	
	public JPanel getBorrowGoodsManage() {
		return borrowGoodsManage;
	}
	
	//搜索侦听器
	class SearchListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			search.getText().trim();//需要进行搜索的模糊物品名
			//执行数据库语句遍历查找rent_table中的数据
		}
	}
	
	//确收按钮侦听器
	class ReceiveBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				Integer.valueOf(table.getValueAt(i, 0).toString().trim());//订单ID
				/**
				 * 执行数据库语句：通过订单ID搜索order_table表中该ID对应的数据的isReturn字段是否为1，
				 * 如果为0则停止该操作；
				 * 如果为1则执行另一数据库语句：在return_table表中添加一条数据：
				 * insert into return_table(damage,o_id) values(0,ID);
				 */
			}else {
				System.out.println("未选中行");
			}
		}
	}
	
	//退回按钮侦听器
	class BackBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				Integer.valueOf(table.getValueAt(i, 0).toString().trim());//订单ID
				/**
				 * 执行数据库语句：通过订单ID搜索order_table表中该ID对应的数据的isReturn字段是否为1，
				 * 如果为0则停止该操作；
				 * 如果为1则执行另一数据库语句：在return_table表中添加一条数据：
				 * insert into return_table(damage,o_id) values(1,ID);
				 */
			}else {
				System.out.println("未选中行");
			}
		}
	}
}
