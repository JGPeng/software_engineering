package com.view.admin;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import main.Constant;
/**
 * 订单管理界面：
 * 分为正在租借订单和结束租借订单；
 * 正在租借订单：指租借方租借的物品仍处在租借时间内且还未归还的订单信息；
 * 结束租借订单：指租借方租借的物品的租借时间已过期自动结束交易，
 * 或仍处于租借时间内但已归还物品且该物品已被检查处理（租借物品管理界面中进行处理）
 */
public class OrderManagePanel {
	private JPanel orderManage,loaning,loaned;
	private JTabbedPane tabbedPane;
	
	public OrderManagePanel() {
		orderManage=new JPanel();
		orderManage.setLayout(new BorderLayout());
		orderManage.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));

		tabbedPane=new JTabbedPane(JTabbedPane.TOP);
		orderManage.add(tabbedPane,BorderLayout.CENTER);
		loaning=new LoaningPanel().getLoaning();
		loaned=new LoanedPanel().getLoaned();
		tabbedPane.add("正在租借订单",loaning);
		tabbedPane.add("结束租借订单",loaned);
	}

	public JPanel getOrderManage() {
		return orderManage;
	}
}
