package com.view.admin;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.util.FitTableColumns;
import main.Constant;
//正在租借订单信息：显示order_table表中所有归还时间为空的数据
public class LoaningPanel {
	private JPanel loaning,searchPanel,mainPanel,btnPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JButton searchBtn,deleteBtn;
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel model;
	private String[] head= {"订单ID","出租物品ID","租借用户ID","租借天数","租借时间","是否发货"};

	public LoaningPanel() {
		loaning=new JPanel();
		loaning.setLayout(new BorderLayout());
		loaning.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));
		
		searchPanel = new JPanel();
		searchPanel.setPreferredSize(new Dimension(0,50));
		loaning.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(null);
		
		searchLabel = new JLabel("输入订单ID进行查询：");
		searchLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		searchLabel.setBounds(10, 10, 150, 30);
		searchPanel.add(searchLabel);
		
		search = new JTextField();
		search.setColumns(10);
		search.setBounds(170, 10, 150, 30);
		searchPanel.add(search);
		
		searchBtn = new JButton("查询");
		searchBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		searchBtn.setBounds(540, 10, 80, 30);
		searchBtn.addActionListener(new SearchListener());
		searchPanel.add(searchBtn);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		loaning.add(mainPanel, BorderLayout.CENTER);

		scrollPane = new JScrollPane();
		table = new JTable();
		model=(DefaultTableModel) table.getModel();
		model.setDataVector(null, head);
		scrollPane.setViewportView(table);
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();//单元格内容居中对齐
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, dtcr);
		new FitTableColumns(table);//根据内容自动调节表格的列宽度
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(0, 50));
		loaning.add(btnPanel, BorderLayout.SOUTH);
		
		deleteBtn = new JButton("删除");
		deleteBtn.setPreferredSize(new Dimension(100, 30));
		deleteBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		deleteBtn.addActionListener(new DeleteBtnListener());
		btnPanel.add(deleteBtn);
	}

	public JPanel getLoaning() {
		return loaning;
	}
	
	//搜索侦听器
	class SearchListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Integer.valueOf(search.getText().trim());//需要进行搜索的模糊物品ID
			//执行数据库语句遍历查找rent_table中归还时间为空的数据
		}
	}
	
	//删除按钮侦听器
	class DeleteBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				Integer.valueOf(table.getValueAt(i, 0).toString().trim());//订单ID
				/**
				 * 执行数据库语句：根据该订单ID删除order_table表中对应的数据。
				 */
			}else {
				System.out.println("未选中行");
			}
		}
	}
}
