package com.view.admin;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.util.FitTableColumns;
import main.Constant;
//未审核物品信息面板
public class NotReviewedPanel {
	private JPanel notReviewed,searchPanel,mainPanel,btnPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JButton searchBtn,adoptBtn,noAdoptBtn;
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel model;
	private String[] head= {"ID","物品名","物品描述","租金/天","押金","数量","出租用户"};
	

	public NotReviewedPanel(){
		notReviewed=new JPanel();
		notReviewed.setLayout(new BorderLayout());
		notReviewed.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));
		
		searchPanel = new JPanel();
		searchPanel.setPreferredSize(new Dimension(0,50));
		notReviewed.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(null);
		
		searchLabel = new JLabel("输入物品名进行查询：");
		searchLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		searchLabel.setBounds(10, 10, 150, 30);
		searchPanel.add(searchLabel);
		
		search = new JTextField();
		search.setColumns(10);
		search.setBounds(170, 10, 150, 30);
		searchPanel.add(search);
		
		searchBtn = new JButton("查询");
		searchBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		searchBtn.setBounds(540, 10, 80, 30);
		searchBtn.addActionListener(new SearchListener());
		searchPanel.add(searchBtn);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		notReviewed.add(mainPanel, BorderLayout.CENTER);

		scrollPane = new JScrollPane();
		table = new JTable();
		model=(DefaultTableModel) table.getModel();
		model.setDataVector(null, head);
		scrollPane.setViewportView(table);
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();//单元格内容居中对齐
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, dtcr);
		new FitTableColumns(table);//根据内容自动调节表格的列宽度
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(0, 50));
		notReviewed.add(btnPanel, BorderLayout.SOUTH);
		
		adoptBtn = new JButton("通过");
		adoptBtn.setPreferredSize(new Dimension(100, 30));
		adoptBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		adoptBtn.addActionListener(new AdoptBtnListener());
		btnPanel.add(adoptBtn);
		
		noAdoptBtn = new JButton("不通过");
		noAdoptBtn.setPreferredSize(new Dimension(100, 30));
		noAdoptBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		noAdoptBtn.addActionListener(new NoAdoptBtnListener());
		btnPanel.add(noAdoptBtn);
	}
	
	public JPanel getAuditApproved() {
		return notReviewed;
	}
	
	//搜索侦听器
	class SearchListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			search.getText().trim();//需要进行搜索的模糊物品名
			//执行数据库语句遍历查找rent_table中的数据
		}
	}
	
	//通过按钮侦听器
	class AdoptBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				Integer.valueOf(table.getValueAt(i, 0).toString().trim());//物品ID
				//执行数据库语句将arent_table表中相应ID的数据传到rent_table表中，并删除原表数据。
			}else {
				System.out.println("未选中行");
			}
		}
	}
	
	//不通过按钮侦听器
	class NoAdoptBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				Integer.valueOf(table.getValueAt(i, 0).toString().trim());//物品ID
				//执行数据库语句将arent_table表中相应ID的数据删除
			}else {
				System.out.println("未选中行");
			}
		}
	}
}
