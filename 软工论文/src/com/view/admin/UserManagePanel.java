package com.view.admin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.util.FitTableColumns;
import com.view.admin.NotReviewedPanel.SearchListener;

import main.Constant;
/**
 * 用户管理界面：
 * 对用户信息进行管理，可查看用户信息、删除用户信息。
 */
public class UserManagePanel {
	private JPanel userManage,searchPanel,mainPanel,btnPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JButton searchBtn,deleteBtn;
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel model;
	private String[] head= {"用户ID","邮箱"};
	

	public UserManagePanel(){
		userManage=new JPanel();
		userManage.setLayout(new BorderLayout());
		userManage.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));
		
		searchPanel = new JPanel();
		searchPanel.setPreferredSize(new Dimension(0,50));
		userManage.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(null);
		
		searchLabel = new JLabel("输入用户ID进行查询：");
		searchLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		searchLabel.setBounds(10, 10, 150, 30);
		searchPanel.add(searchLabel);
		
		search = new JTextField();
		search.setColumns(10);
		search.setBounds(170, 10, 150, 30);
		searchPanel.add(search);
		
		searchBtn = new JButton("查询");
		searchBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		searchBtn.setBounds(540, 10, 80, 30);
		searchBtn.addActionListener(new SearchListener());
		searchPanel.add(searchBtn);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		userManage.add(mainPanel, BorderLayout.CENTER);

		scrollPane = new JScrollPane();
		table = new JTable();
		model=(DefaultTableModel) table.getModel();
		model.setDataVector(null, head);
		scrollPane.setViewportView(table);
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();//单元格内容居中对齐
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, dtcr);
		new FitTableColumns(table);//根据内容自动调节表格的列宽度
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(0, 50));
		userManage.add(btnPanel, BorderLayout.SOUTH);
		
		deleteBtn = new JButton("删除用户");
		deleteBtn.setPreferredSize(new Dimension(100, 30));
		deleteBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		deleteBtn.addActionListener(new DeleteBtnListener());
		btnPanel.add(deleteBtn);
	}
	
	public JPanel getUserManage() {
		return userManage;
	}
	
	//搜索侦听器
	class SearchListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Integer.valueOf(search.getText().trim());//需要进行搜索的模糊物品ID
			//执行数据库语句遍历查找rent_table中归还时间为空的数据
		}
	}
	
	//删除按钮侦听器
	class DeleteBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				Integer.valueOf(table.getValueAt(i, 0).toString().trim());//用户ID
				/**
				 * 执行数据库语句：通过用户ID搜索user_table表中该ID对应的数据，并删除该数据。
				 */
			}else {
				System.out.println("未选中行");
			}
		}
	}
}
