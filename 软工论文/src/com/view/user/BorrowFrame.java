package com.view.user;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import com.model.Rent;

import main.Constant;
public class BorrowFrame {
	private JFrame borrow=null;
	private JLabel nameLabel,descriptionLabel,priceLabel,securityLabel,daysLabel;
	private JLabel name,price,security;
	private JTextArea description;
	private JTextField days;
	private JButton borrowBtn;
	private Rent rent=null;
	
	public BorrowFrame() {
		borrow=new JFrame("租借信息单确认表");
		borrow.setSize(Constant.MESSAGE_WIDTH,Constant.MESSAGE_HEIGHT);
		borrow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		borrow.setResizable(false);
		borrow.setLocationRelativeTo(null);
		borrow.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("租借物品订单信息",JLabel.CENTER);
		lblNewLabel.setFont(new Font("宋体", Font.BOLD, 25));
		lblNewLabel.setBounds(0, 0, 370, 40);
		borrow.getContentPane().add(lblNewLabel);
		
		nameLabel = new JLabel("物品名称：");
		nameLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		nameLabel.setBounds(5, 50, 100, 30);
		borrow.getContentPane().add(nameLabel);
		
		name = new JLabel();
		name.setFont(new Font("宋体", Font.PLAIN, 14));
		name.setBounds(108, 50, 100, 30);
		borrow.getContentPane().add(name);
		
		descriptionLabel = new JLabel("物品描述：");
		descriptionLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		descriptionLabel.setBounds(5, 90, 100, 30);
		borrow.getContentPane().add(descriptionLabel);
		
		description = new JTextArea();
		description.setRows(3);
		description.setBackground(SystemColor.control);
		description.setEditable(false);
		description.setFont(new Font("宋体", Font.PLAIN, 14));
		description.setBounds(108, 90, 239, 55);
		borrow.getContentPane().add(description);
		
		priceLabel = new JLabel("租金/天：");
		priceLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		priceLabel.setBounds(5, 150, 100, 30);
		borrow.getContentPane().add(priceLabel);
		
		price = new JLabel();
		price.setFont(new Font("宋体", Font.PLAIN, 14));
		price.setBounds(108, 150, 100, 30);
		borrow.getContentPane().add(price);
		
		securityLabel = new JLabel("押金：");
		securityLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		securityLabel.setBounds(5, 190, 100, 30);
		borrow.getContentPane().add(securityLabel);
		
		security = new JLabel();
		security.setFont(new Font("宋体", Font.PLAIN, 14));
		security.setBounds(108, 190, 100, 30);
		borrow.getContentPane().add(security);
		
		daysLabel = new JLabel("租借天数：");
		daysLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		daysLabel.setBounds(5, 230, 100, 30);
		borrow.getContentPane().add(daysLabel);
		
		days = new JTextField();
		days.setColumns(10);
		days.setBounds(108, 230, 100, 30);
		borrow.getContentPane().add(days);
		
		borrowBtn = new JButton("确认租借");
		borrowBtn.setFont(new Font("宋体", Font.BOLD, 14));
		borrowBtn.setBounds(125, 280, 120, 30);
		borrowBtn.addActionListener(new BorrowBtnListener());
		borrow.getContentPane().add(borrowBtn);
		
		borrow.setVisible(true);
	}

	public JFrame getBorrow() {
		return borrow;
	}
	
	//确认租借按钮侦听器
	class BorrowBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			System.out.println("确认租借");
		}
	}

	public void setRent(Rent rent) {
		this.rent=rent;
		name.setText(rent.getName());
		description.setText(rent.getDescription());
		price.setText(String.valueOf(rent.getPrice()));
		security.setText(String.valueOf(rent.getSecurity()));
	}
}
