package com.view.user;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.model.Rent;
import com.view.user.DeleteGoodsFrame.ConfirmListener;

import main.Constant;
public class AddGoodsFrame {
	private JFrame addGoods=null;
	private JLabel nameLabel,descriptionLabel,priceLabel,securityLabel,numLabel;
	private JTextField name,price,security,num;
	private JTextArea description;
	private JButton confirmBtn,rewriteBtn;
	private Rent rent;
	
	public AddGoodsFrame() {
		addGoods=new JFrame("添加出租物品信息窗口");
		addGoods.setSize(Constant.MESSAGE_WIDTH,Constant.MESSAGE_HEIGHT);
		addGoods.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addGoods.setResizable(false);
		addGoods.setLocationRelativeTo(null);
		addGoods.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("添加出租物品信息",JLabel.CENTER);
		lblNewLabel.setFont(new Font("宋体", Font.BOLD, 25));
		lblNewLabel.setBounds(0, 0, 370, 40);
		addGoods.getContentPane().add(lblNewLabel);
		
		nameLabel = new JLabel("物品名称：");
		nameLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		nameLabel.setBounds(5, 50, 100, 30);
		addGoods.getContentPane().add(nameLabel);
		
		name = new JTextField("");
		name.setFont(new Font("宋体", Font.PLAIN, 14));
		name.setBounds(108, 50, 100, 30);
		addGoods.getContentPane().add(name);
		
		descriptionLabel = new JLabel("物品描述：");
		descriptionLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		descriptionLabel.setBounds(5, 91, 100, 30);
		addGoods.getContentPane().add(descriptionLabel);
		
		description = new JTextArea();
		description.setLineWrap(true);
		description.setBackground(SystemColor.textHighlightText);
		description.setFont(new Font("宋体", Font.PLAIN, 14));
		description.setBounds(108, 90, 200, 55);
		addGoods.getContentPane().add(description);
		
		priceLabel = new JLabel("租金/天：");
		priceLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		priceLabel.setBounds(5, 150, 100, 30);
		addGoods.getContentPane().add(priceLabel);
		
		price = new JTextField("");
		price.setFont(new Font("宋体", Font.PLAIN, 14));
		price.setBounds(108, 150, 100, 30);
		addGoods.getContentPane().add(price);
		
		securityLabel = new JLabel("押金：");
		securityLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		securityLabel.setBounds(5, 190, 100, 30);
		addGoods.getContentPane().add(securityLabel);
		
		security = new JTextField("");
		security.setFont(new Font("宋体", Font.PLAIN, 14));
		security.setBounds(108, 190, 100, 30);
		addGoods.getContentPane().add(security);
		
		numLabel = new JLabel("数量：");
		numLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		numLabel.setBounds(5, 225, 100, 30);
		addGoods.getContentPane().add(numLabel);
		
		num = new JTextField("");
		num.setColumns(10);
		num.setBounds(108, 226, 100, 30);
		addGoods.getContentPane().add(num);
		
		confirmBtn = new JButton("提交申请");
		confirmBtn.setFont(new Font("宋体", Font.BOLD, 14));
		confirmBtn.setBounds(55, 270, 120, 30);
		confirmBtn.addActionListener(new ConfirmBtnListener());
		addGoods.getContentPane().add(confirmBtn);
		
		rewriteBtn = new JButton("重置");
		rewriteBtn.setFont(new Font("宋体", Font.BOLD, 14));
		rewriteBtn.setBounds(190, 270, 120, 30);
		rewriteBtn.addActionListener(new RewriteBtnListener());
		addGoods.getContentPane().add(rewriteBtn);
		
		addGoods.setVisible(true);
	}

	public JFrame getAddGoods() {
		return addGoods;
	}

	//确认添加按钮侦听器
	class ConfirmBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(!name.getText().trim().equals("") || !description.getText().trim().equals("")
					|| !price.getText().trim().equals("") || !security.getText().trim().equals("")
					|| !num.getText().trim().equals("")) {
				rent.setName(name.getText().trim());
				rent.setDescription(description.getText().trim());
				rent.setPrice(Double.valueOf(price.getText().trim()));
				rent.setSecurity(Double.valueOf(security.getText().trim()));
				rent.setNum(Integer.valueOf(num.getText().trim()));
			}
			System.out.println("确认添加");
		}
	}
	
	//重置按钮侦听器
	class RewriteBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(!name.getText().equals("")) name.setText("");
			if(!description.getText().equals("")) description.setText("");
			if(!price.getText().equals("")) price.setText("");
			if(!security.getText().equals("")) security.setText("");
			if(!num.getText().equals("")) num.setText("");
		}
	}
}
