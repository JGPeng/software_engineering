package com.view.user;

import javax.swing.JFrame;
import main.Constant;
import javax.swing.JLabel;

import com.model.Rent;

import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class DeleteGoodsFrame {
	private JFrame deleteGoods=null;
	private JLabel title;
	private JButton confirmBtn,cancelBtn;
	private Rent rent=null;

	public DeleteGoodsFrame() {
		deleteGoods=new JFrame("信息框");
		deleteGoods.setSize(Constant.MESSAGE_WIDTH,Constant.MESSAGE_HEIGHT/2);
		deleteGoods.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		deleteGoods.setResizable(false);
		deleteGoods.setLocationRelativeTo(null);
		deleteGoods.getContentPane().setLayout(null);
		
		title = new JLabel("确认删除？",JLabel.CENTER);
		title.setFont(new Font("宋体", Font.BOLD, 16));
		title.setBounds(0,0,390, 80);
		deleteGoods.getContentPane().add(title);
		
		confirmBtn = new JButton("确认");
		confirmBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		confirmBtn.setBounds(59, 100, 100, 30);
		confirmBtn.addActionListener(new ConfirmListener());
		deleteGoods.getContentPane().add(confirmBtn);
		
		cancelBtn = new JButton("取消");
		cancelBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		cancelBtn.setBounds(201, 100, 100, 30);
		cancelBtn.addActionListener(new CencelListener());
		deleteGoods.getContentPane().add(cancelBtn);
	}

	public JFrame getDeleteGoods() {
		return deleteGoods;
	}
	
	public void setRent(Rent rent) {
		this.rent = rent;
	}

	//确认删除按钮
	class ConfirmListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			System.out.println("确认删除");
		}
	}
	
	//取消删除按钮
	class CencelListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			deleteGoods.setVisible(false);
		}
	}
}
