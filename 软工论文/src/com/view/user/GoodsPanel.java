package com.view.user;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.model.Rent;
import com.util.FitTableColumns;
import main.Constant;
/**
 * 物品信息界面：
 * 显示所有课出租物品信息，点击列表中需要租借的物品，然后点击租借按钮会跳出订单信息窗口，
 * 查看信息是否正确并填写租借天数即可进行租借。
 */
public class GoodsPanel {
	private JPanel goods=null,searchPanel,mainPanel,btnPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JButton searchBtn,borrowBtn;
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel model;
	private String[] head= {"ID","物品名","描述","租金/天","押金","剩余数量"};
	private BorrowFrame borrowFrame=null;
	private Rent rent=null;
	private String[] datas= {"1","吉他","乐器","10","600","2"};

	public GoodsPanel() {
		goods=new JPanel();
		goods.setLayout(new BorderLayout());
		goods.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));
		
		searchPanel = new JPanel();
		searchPanel.setPreferredSize(new Dimension(0,50));
		goods.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(null);
		
		searchLabel = new JLabel("输入物品名进行查询：");
		searchLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		searchLabel.setBounds(10, 10, 150, 30);
		searchPanel.add(searchLabel);
		
		search = new JTextField();
		search.setColumns(10);
		search.setBounds(170, 10, 150, 30);
		searchPanel.add(search);
		
		searchBtn = new JButton("查询");
		searchBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		searchBtn.setBounds(520, 10, 100, 30);
		searchBtn.addActionListener(new SearchListener());
		searchPanel.add(searchBtn);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		goods.add(mainPanel, BorderLayout.CENTER);

		scrollPane = new JScrollPane();
		table = new JTable();
		model=(DefaultTableModel) table.getModel();
		model.setDataVector(null, head);
		for(int i=0;i<10;i++) model.addRow(datas);
		scrollPane.setViewportView(table);
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();//单元格内容居中对齐
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, dtcr);
		new FitTableColumns(table);//根据内容自动调节表格的列宽度
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(0, 80));
		goods.add(btnPanel, BorderLayout.SOUTH);
		
		borrowBtn = new JButton("租借");
		borrowBtn.setPreferredSize(new Dimension(100, 40));
		borrowBtn.setFont(new Font("宋体", Font.PLAIN, 16));
		borrowBtn.addActionListener(new BorrowBtnListener());
		btnPanel.add(borrowBtn);
	}
	
	public JPanel getGoods() {
		return goods;
	}
	
	//搜索侦听器
	class SearchListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			
		}
	}
	
	//租借按钮侦听器
	class BorrowBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				if(Integer.valueOf(table.getValueAt(i, 5).toString().trim())>0) {  //剩余数量大于0时可租借
					if(rent==null) {
						rent=new Rent();
					}
					rent.setId(Integer.valueOf(table.getValueAt(i, 0).toString().trim()));
					rent.setName(table.getValueAt(i, 1).toString().trim());
					rent.setDescription(table.getValueAt(i, 2).toString().trim());
					rent.setPrice(Double.valueOf(table.getValueAt(i, 3).toString().trim()));
					rent.setSecurity(Double.valueOf(table.getValueAt(i, 4).toString().trim()));
					if(borrowFrame==null) {
						borrowFrame=new BorrowFrame();
					}
					borrowFrame.setRent(rent);
					borrowFrame.getBorrow().setVisible(true);
				}
			}
		}
	}
}
