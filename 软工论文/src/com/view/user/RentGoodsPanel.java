package com.view.user;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.model.Rent;
import com.util.FitTableColumns;
import main.Constant;
/**
 * 物品出租界面
 * 显示自己的出租物品的信息，其中包括正在申请中的和已申请通过了的；
 * 用户可以对正在申请中的物品信息就行修改、删除，但不能修改已申请通过的物品信息，
 * 用户还可以申请新的出租物品，填写相关数据并提交申请即可。
 */
public class RentGoodsPanel {
	private JPanel rentGoods,searchPanel,mainPanel,btnPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JButton searchBtn,changeBtn,deleteBtn,addBtn;
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel model;
	private String[] head= {"物品名","描述","租金/天","押金","数量","申请通过"};
	private ChangeGoodsFrame changeFrame=null;
	private DeleteGoodsFrame deleteFrame=null;
	private AddGoodsFrame addFrame=null;
	private String[] datas= {"吉他","乐器","10","600","2","0"};
	private Rent rent=null;
	
	public RentGoodsPanel() {
		rentGoods=new JPanel();
		rentGoods.setLayout(new BorderLayout(0, 0));
		rentGoods.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));

		searchPanel = new JPanel();
		searchPanel.setPreferredSize(new Dimension(0,50));
		rentGoods.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(null);
		
		searchLabel = new JLabel("输入物品名进行查询：");
		searchLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		searchLabel.setBounds(10, 10, 150, 30);
		searchPanel.add(searchLabel);
		
		search = new JTextField();
		search.setColumns(10);
		search.setBounds(170, 10, 150, 30);
		searchPanel.add(search);
		
		searchBtn = new JButton("查询");
		searchBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		searchBtn.setBounds(520, 10, 100, 30);
		searchBtn.addActionListener(new SearchListener());
		searchPanel.add(searchBtn);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		rentGoods.add(mainPanel, BorderLayout.CENTER);

		scrollPane = new JScrollPane();
		table = new JTable();
		model=(DefaultTableModel) table.getModel();
		model.setDataVector(null, head);
		for(int i=0;i<10;i++) model.addRow(datas);
		scrollPane.setViewportView(table);
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();//单元格内容居中对齐
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, dtcr);
		new FitTableColumns(table);//根据内容自动调节表格的列宽度
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(0, 80));
		rentGoods.add(btnPanel, BorderLayout.SOUTH);
		
		changeBtn = new JButton("修改出租物品信息");
		changeBtn.setPreferredSize(new Dimension(150, 30));
		changeBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		changeBtn.addActionListener(new ChangeBtnListener());
		btnPanel.add(changeBtn);

		deleteBtn = new JButton("删除出租物品信息");
		deleteBtn.setPreferredSize(new Dimension(150, 30));
		deleteBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		deleteBtn.addActionListener(new DeleteBtnListener());
		btnPanel.add(deleteBtn);

		addBtn = new JButton("添加出租物品信息");
		addBtn.setPreferredSize(new Dimension(150, 30));
		addBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		addBtn.addActionListener(new AddBtnListener());
		btnPanel.add(addBtn);
	}

	public JPanel getRentGoods() {
		return rentGoods;
	}
	
	//搜索侦听器
	class SearchListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
		}
	}
	
	//修改出租物品信息按钮侦听器
	class ChangeBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				if(Integer.valueOf(table.getValueAt(i, 5).toString().trim())==0) {  //只有未通过申请的信息才能进行修改
					if(rent==null) {
						rent=new Rent();
					}
					rent.setName(table.getValueAt(i, 0).toString().trim());
					rent.setDescription(table.getValueAt(i, 1).toString().trim());
					rent.setPrice(Double.valueOf(table.getValueAt(i, 2).toString().trim()));
					rent.setSecurity(Double.valueOf(table.getValueAt(i, 3).toString().trim()));
					rent.setNum(Integer.valueOf(table.getValueAt(i, 4).toString().trim()));
					if(changeFrame==null) {
						changeFrame=new ChangeGoodsFrame();
					}
					changeFrame.setRent(rent);
					changeFrame.getChangeGoods().setVisible(true);
				}
			}
		}
	}
	
	//删除出租物品信息按钮侦听器
	class DeleteBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {
				if(rent==null) {
					rent=new Rent();
				}
				rent.setName(table.getValueAt(i, 0).toString().trim());
				if(deleteFrame==null) {
					deleteFrame=new DeleteGoodsFrame();
				}
				deleteFrame.setRent(rent);
				deleteFrame.getDeleteGoods().setVisible(true);
			}
		}
	}
	
	//添加出租物品按钮侦听器
	class AddBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(addFrame==null) {
				addFrame=new AddGoodsFrame();
			}
			addFrame.getAddGoods().setVisible(true);
		}
	}
}
