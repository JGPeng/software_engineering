package com.view.user;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.view.user.ReturnFrame.CancelBtnListener;
import com.view.user.ReturnFrame.ConfirmBtnListener;

import main.Constant;
public class ConfirmFrame {
	private JFrame confirm=null;
	private JLabel title;
	private JButton confirmBtn,cancelBtn;
	private Integer id;

	public ConfirmFrame() {
		confirm=new JFrame("确认收货信息框");
		confirm.setSize(Constant.MESSAGE_WIDTH,Constant.MESSAGE_HEIGHT/2);
		confirm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		confirm.setResizable(false);
		confirm.setLocationRelativeTo(null);
		confirm.getContentPane().setLayout(null);
		
		title = new JLabel("确认收货？",JLabel.CENTER);
		title.setFont(new Font("宋体", Font.BOLD, 16));
		title.setBounds(0,0,390, 80);
		confirm.getContentPane().add(title);
		
		confirmBtn = new JButton("确认");
		confirmBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		confirmBtn.setBounds(59, 100, 100, 30);
		confirmBtn.addActionListener(new ConfirmBtnListener());
		confirm.getContentPane().add(confirmBtn);
		
		cancelBtn = new JButton("取消");
		cancelBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		cancelBtn.setBounds(201, 100, 100, 30);
		cancelBtn.addActionListener(new CancelBtnListener());
		confirm.getContentPane().add(cancelBtn);
	}

	public JFrame getDeleteGoods() {
		return confirm;
	}
	
	//确认收货按钮侦听器
	class ConfirmBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
		}
	}
	
	//取消收货按钮侦听器
	class CancelBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			confirm.setVisible(false);
		}
	}

	public void setID(Integer id) {
		this.id=id;
	}
}
