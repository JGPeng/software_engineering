package com.view.user;

import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.model.Rent;

import main.Constant;
public class ChangeGoodsFrame {
	private JFrame changeGoods=null;
	private JLabel nameLabel,descriptionLabel,priceLabel,securityLabel,numLabel;
	private JTextField name,price,security,num;
	private JTextArea description;
	private JButton confirm;
	private Rent rent=null;
	
	public ChangeGoodsFrame() {
		changeGoods=new JFrame("修改出租物品信息窗口");
		changeGoods.setSize(Constant.MESSAGE_WIDTH,Constant.MESSAGE_HEIGHT);
		changeGoods.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		changeGoods.setResizable(false);
		changeGoods.setLocationRelativeTo(null);
		changeGoods.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("修改出租物品信息",JLabel.CENTER);
		lblNewLabel.setFont(new Font("宋体", Font.BOLD, 25));
		lblNewLabel.setBounds(0, 0, 370, 40);
		changeGoods.getContentPane().add(lblNewLabel);
		
		nameLabel = new JLabel("物品名称：");
		nameLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		nameLabel.setBounds(5, 50, 100, 30);
		changeGoods.getContentPane().add(nameLabel);
		
		name = new JTextField();
		name.setFont(new Font("宋体", Font.PLAIN, 14));
		name.setBounds(108, 50, 100, 30);
		changeGoods.getContentPane().add(name);
		
		descriptionLabel = new JLabel("物品描述：");
		descriptionLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		descriptionLabel.setBounds(5, 91, 100, 30);
		changeGoods.getContentPane().add(descriptionLabel);
		
		description = new JTextArea();
		description.setRows(3);
		description.setBackground(SystemColor.textHighlightText);
		description.setFont(new Font("宋体", Font.PLAIN, 14));
		description.setBounds(108, 90, 239, 55);
		changeGoods.getContentPane().add(description);
		
		priceLabel = new JLabel("租金/天：");
		priceLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		priceLabel.setBounds(5, 150, 100, 30);
		changeGoods.getContentPane().add(priceLabel);
		
		price = new JTextField();
		price.setFont(new Font("宋体", Font.PLAIN, 14));
		price.setBounds(108, 150, 100, 30);
		changeGoods.getContentPane().add(price);
		
		securityLabel = new JLabel("押金：");
		securityLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		securityLabel.setBounds(5, 190, 100, 30);
		changeGoods.getContentPane().add(securityLabel);
		
		security = new JTextField();
		security.setFont(new Font("宋体", Font.PLAIN, 14));
		security.setBounds(108, 190, 100, 30);
		changeGoods.getContentPane().add(security);
		
		numLabel = new JLabel("数量：");
		numLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		numLabel.setBounds(5, 225, 100, 30);
		changeGoods.getContentPane().add(numLabel);
		
		num = new JTextField();
		num.setColumns(10);
		num.setBounds(108, 226, 100, 30);
		changeGoods.getContentPane().add(num);
		
		confirm = new JButton("确认修改");
		confirm.setFont(new Font("宋体", Font.BOLD, 14));
		confirm.setBounds(125, 281, 120, 30);
		confirm.addActionListener(new ConfirmListener());
		changeGoods.getContentPane().add(confirm);
		
		changeGoods.setVisible(true);
	}

	public JFrame getChangeGoods() {
		return changeGoods;
	}
	
	public void setRent(Rent rent) {
		this.rent = rent;
		name.setText(rent.getName());
		description.setText(rent.getDescription());
		price.setText(String.valueOf(rent.getPrice()));
		security.setText(String.valueOf(rent.getSecurity()));
		num.setText(String.valueOf(rent.getNum()));
	}

	//确认修改按钮
	class ConfirmListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			System.out.println("确认修改");
		}
	}
}
