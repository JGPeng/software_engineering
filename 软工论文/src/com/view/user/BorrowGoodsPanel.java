package com.view.user;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import com.util.FitTableColumns;
import main.Constant;
/**
 * 物品租借界面：
 * 显示已租借订单信息，其中包括订单ID、物品名、租金/天、押金、租借天数、金额、租借时间、归还时间、是否收到货、是否归还；
 * 如果收到货则点击确认收货，如果要归还则点击归还物品。
 */
public class BorrowGoodsPanel {
	private JPanel borrowGoods=null,searchPanel,mainPanel,btnPanel;
	private JLabel searchLabel;
	private JTextField search;
	private JButton searchBtn,confirmBtn,returnBtn;
	private JScrollPane scrollPane;
	private JTable table;
	private DefaultTableModel model;
	private String[] head= {"订单ID","物品名","租金/天","押金","金额","租借时间","归还时间","收到货","归还"};
	private ConfirmFrame confirmFrame=null;
	private ReturnFrame returnFrame=null;
	
	public BorrowGoodsPanel() {
		borrowGoods=new JPanel();
		borrowGoods.setLayout(new BorderLayout());
		borrowGoods.setPreferredSize(new Dimension(Constant.WIDTH-Constant.BTN_WIDTH-15, Constant.HEIGHT-Constant.TITLE_HEIGHT));
		
		searchPanel = new JPanel();
		searchPanel.setPreferredSize(new Dimension(0,50));
		borrowGoods.add(searchPanel, BorderLayout.NORTH);
		searchPanel.setLayout(null);
		
		searchLabel = new JLabel("输入物品名进行查询：");
		searchLabel.setFont(new Font("宋体", Font.PLAIN, 14));
		searchLabel.setBounds(10, 10, 150, 30);
		searchPanel.add(searchLabel);
		
		search = new JTextField();
		search.setColumns(10);
		search.setBounds(170, 10, 150, 30);
		searchPanel.add(search);
		
		searchBtn = new JButton("查询");
		searchBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		searchBtn.setBounds(520, 10, 100, 30);
		searchBtn.addActionListener(new SearchListener());
		searchPanel.add(searchBtn);
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		borrowGoods.add(mainPanel, BorderLayout.CENTER);

		scrollPane = new JScrollPane();
		table = new JTable();
		model=(DefaultTableModel) table.getModel();
		model.setDataVector(null, head);
//		for(int i=0;i<50;i++) model.addRow(s);
		scrollPane.setViewportView(table);
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();//单元格内容居中对齐
		dtcr.setHorizontalAlignment(JLabel.CENTER);
		table.setDefaultRenderer(Object.class, dtcr);
		new FitTableColumns(table);//根据内容自动调节表格的列宽度
		mainPanel.add(scrollPane,BorderLayout.CENTER);
		
		btnPanel = new JPanel();
		btnPanel.setPreferredSize(new Dimension(0, 80));
		borrowGoods.add(btnPanel, BorderLayout.SOUTH);
		
		confirmBtn = new JButton("确认收货");
		confirmBtn.setPreferredSize(new Dimension(100, 30));
		confirmBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		confirmBtn.addActionListener(new ConfirmBtnListener());
		btnPanel.add(confirmBtn);
		
		returnBtn = new JButton("归还物品");
		returnBtn.setPreferredSize(new Dimension(100, 30));
		returnBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		returnBtn.addActionListener(new ReturnBtnListener());
		btnPanel.add(returnBtn);
	}

	public JPanel getBorrowGoods() {
		return borrowGoods;
	}
	
	//搜索侦听器
	class SearchListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			search.getText().trim();//需要进行搜索的模糊物品名
			//执行数据库语句遍历查找rent_table中的数据
		}
	}
	
	//确认收货按钮侦听器
	class ConfirmBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				if(Integer.valueOf(table.getValueAt(i, 7).toString().trim())==0) {
					if(confirmFrame==null) {
						confirmFrame=new ConfirmFrame();
					}
					confirmFrame.setID(Integer.valueOf(table.getValueAt(i, 0).toString().trim()));
					confirmFrame.getDeleteGoods().setVisible(true);
				}else {
					System.out.println("该物品已确收");
				}
			}else {
				System.out.println("未选中行");
			}
		}
	}
	
	//归还物品按钮侦听器
	class ReturnBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int i=table.getSelectedRow();
			if(i>=0) {  //选中行
				if(Integer.valueOf(table.getValueAt(i, 8).toString().trim())==0) {
					if(returnFrame==null) {
						returnFrame=new ReturnFrame();
					}
					returnFrame.setID(Integer.valueOf(table.getValueAt(i, 0).toString().trim()));
					returnFrame.getReturnGoods().setVisible(true);
				}else {
					System.out.println("该物品已归还");
				}
			}else {
				System.out.println("未选中行");
			}
		}
	}
}
