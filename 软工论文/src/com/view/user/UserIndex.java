package com.view.user;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.*;
import main.Constant;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;
//用户系统界面
public class UserIndex {
	private JFrame user;
	private JPanel titlePanel,centerPanel,buttonPanel;
	private JTabbedPane mainPanel;
	private JLabel title;
	private JButton goodsBtn,rentGoodsBtn,borrowGoodsBtn;
	
	public UserIndex() {
		user=new JFrame("用户界面");
		user.setSize(Constant.WIDTH,Constant.HEIGHT);
		user.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		user.setResizable(false);
		user.setLocationRelativeTo(null);
		
		titlePanel = new JPanel();
		titlePanel.setBackground(new Color(0, 204, 255));
		titlePanel.setPreferredSize(new Dimension(Constant.TITLE_WIDTH, Constant.TITLE_HEIGHT));
		user.getContentPane().add(titlePanel, BorderLayout.NORTH);
		titlePanel.setLayout(null);
		
		title = new JLabel("用户界面",JLabel.CENTER);
		title.setFont(new Font("宋体", Font.BOLD, 26));
		title.setBounds(0, 0, 790, 60);
		titlePanel.add(title);
		
		centerPanel = new JPanel();
		centerPanel.setBackground(new Color(153, 204, 255));
		user.getContentPane().add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new BorderLayout(0, 0));
		
		buttonPanel = new JPanel();
		buttonPanel.setBackground(new Color(102, 204, 255));
		buttonPanel.setPreferredSize(new Dimension(Constant.BTN_WIDTH, Constant.BTN_HEIGHT));
		centerPanel.add(buttonPanel, BorderLayout.WEST);
		
		goodsBtn = new JButton("物品信息界面");
		goodsBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		goodsBtn.setPreferredSize(new Dimension(120, 30));
		goodsBtn.addActionListener(new GoodsBtnListener());
		buttonPanel.add(goodsBtn);
		
		rentGoodsBtn = new JButton("物品出租界面");
		rentGoodsBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		rentGoodsBtn.setPreferredSize(new Dimension(120, 30));
		rentGoodsBtn.addActionListener(new RentGoodsBtnListener());
		buttonPanel.add(rentGoodsBtn);
		
		borrowGoodsBtn = new JButton("物品租借界面");
		borrowGoodsBtn.setFont(new Font("宋体", Font.PLAIN, 14));
		borrowGoodsBtn.setPreferredSize(new Dimension(120, 30));
		borrowGoodsBtn.addActionListener(new BorrowGoodsBtnListener());
		buttonPanel.add(borrowGoodsBtn);
		
		mainPanel = new JTabbedPane();
		mainPanel.setBackground(new Color(204, 204, 255));
		centerPanel.add(mainPanel, BorderLayout.EAST);
		mainPanel.add("物品信息界面",new GoodsPanel().getGoods());
		mainPanel.add("物品出租界面",new RentGoodsPanel().getRentGoods());
		mainPanel.add("物品租借界面",new BorrowGoodsPanel().getBorrowGoods());
		
		user.setVisible(true);
	}

	
	public JFrame getUser() {
		return user;
	}
	
	//物品信息界面按钮侦听器
	class GoodsBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			mainPanel.setSelectedIndex(0);
		}
	}
	
	//物品出租界面按钮侦听器
	class RentGoodsBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			mainPanel.setSelectedIndex(1);
		}
	}
	
	//物品租借界面按钮侦听器
	class BorrowGoodsBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			mainPanel.setSelectedIndex(2);
		}
	}
}
