package com.model;

public class Return {
	private Integer id;
	private Integer damage;
	private Integer o_id;
	
	public Return() {
	}
	public Return(Integer id, Integer damage, Integer o_id) {
		this.id = id;
		this.damage = damage;
		this.o_id = o_id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getDamage() {
		return damage;
	}
	public void setDamage(Integer damage) {
		this.damage = damage;
	}
	public Integer getO_id() {
		return o_id;
	}
	public void setO_id(Integer o_id) {
		this.o_id = o_id;
	}
}
