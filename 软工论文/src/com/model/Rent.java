package com.model;

public class Rent {
	private Integer id;
	private String name;
	private String description;
	private double price;
	private double security;
	private Integer num;
	private Integer u_id;
	
	public Rent() {
	}
	public Rent(Integer id, String name, String description, double price, double security, Integer num) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.security = security;
		this.num = num;
	}
	public Rent(Integer id, String name, String description, double price, double security, Integer num, Integer u_id) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.security = security;
		this.num = num;
		this.u_id = u_id;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getSecurity() {
		return security;
	}
	public void setSecurity(double security) {
		this.security = security;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getU_id() {
		return u_id;
	}
	public void setU_id(Integer u_id) {
		this.u_id = u_id;
	}
}
