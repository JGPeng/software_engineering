package com.model;

public class User {
	private Integer uid;
	private String upassword;
	private String email;
	
	public User() {
	}
	public User(Integer uid, String upassword, String email) {
		this.uid = uid;
		this.upassword = upassword;
		this.email = email;
	}
	public Integer getUid() {
		return uid;
	}
	public void setUid(Integer uid) {
		this.uid = uid;
	}
	public String getUpassword() {
		return upassword;
	}
	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
