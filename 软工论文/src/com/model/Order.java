package com.model;

import java.sql.Timestamp;

public class Order {
	private Integer id;
	private Integer r_id;
	private Integer u_id;
	private Integer days;
	private Timestamp retal_time;
	private Timestamp return_time;
	private Integer isSend;
	private Integer isReceipt;
	private Integer isReturn;
	
	public Order() {
	}
	public Order(Integer id, Integer r_id, Integer u_id, Integer days, Timestamp retal_time, Timestamp return_time,
			Integer isSend, Integer isReceipt, Integer isReturn) {
		this.id = id;
		this.r_id = r_id;
		this.u_id = u_id;
		this.days = days;
		this.retal_time = retal_time;
		this.return_time = return_time;
		this.isSend = isSend;
		this.isReceipt = isReceipt;
		this.isReturn = isReturn;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getR_id() {
		return r_id;
	}
	public void setR_id(Integer r_id) {
		this.r_id = r_id;
	}
	public Integer getU_id() {
		return u_id;
	}
	public void setU_id(Integer u_id) {
		this.u_id = u_id;
	}
	public Integer getDays() {
		return days;
	}
	public void setDays(Integer days) {
		this.days = days;
	}
	public Timestamp getRetal_time() {
		return retal_time;
	}
	public void setRetal_time(Timestamp retal_time) {
		this.retal_time = retal_time;
	}
	public Timestamp getReturn_time() {
		return return_time;
	}
	public void setReturn_time(Timestamp return_time) {
		this.return_time = return_time;
	}
	public Integer getIsSend() {
		return isSend;
	}
	public void setIsSend(Integer isSend) {
		this.isSend = isSend;
	}
	public Integer getIsReceipt() {
		return isReceipt;
	}
	public void setIsReceipt(Integer isReceipt) {
		this.isReceipt = isReceipt;
	}
	public Integer getIsReturn() {
		return isReturn;
	}
	public void setIsReturn(Integer isReturn) {
		this.isReturn = isReturn;
	}
}
