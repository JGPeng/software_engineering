package main;

public class Constant {
	public static final int WIDTH=800;//系统界面宽度
	public static final int HEIGHT=600;//系统界面高度
	
	public static final int LOGIN_WIDTH=380;//登陆界面宽度
	public static final int LOGIN_HEIGHT=350;//登陆界面高度

	public static final int TITLE_WIDTH=WIDTH-10;//系统界面标题面板宽度
	public static final int TITLE_HEIGHT=60;//系统界面标题面板高度
	
	public static final int BTN_WIDTH=150;//系统界面按钮面板宽度
	public static final int BTN_HEIGHT=HEIGHT-TITLE_HEIGHT;//系统界面按钮面板高度

	public static final int MESSAGE_WIDTH=400;//信息表窗口宽度
	public static final int MESSAGE_HEIGHT=350;//信息表窗口高度
	
}
